$(document).ready(function(){

	// Hamburger
    $("#hamburger").on('click', function(e){
        $(".header_dropdown").slideToggle();
        $(".header_dropdown").css('display', 'flex');
        $(".bar1").toggleClass('change1');
        $(".bar2").toggleClass('change2');
        $(".bar3").toggleClass('change3');
    });

 
 	// Floor Plans
 	$('#floor_one').click(function(){
 		$('.floor_choice').removeClass('active');
 		$(this).addClass('active');
 		$('.floor_plan').slideUp();
 		$('#floor_plan_one').slideDown();
 	});
 	$('#floor_two').click(function(){
 		$('.floor_choice').removeClass('active');
 		$(this).addClass('active');
 		$('.floor_plan').slideUp();
 		$('#floor_plan_two').slideDown();
 	});
 	$('#floor_three').click(function(){
 		$('.floor_choice').removeClass('active');
 		$(this).addClass('active');
 		$('.floor_plan').slideUp();
 		$('#floor_plan_three').slideDown();
 	});
 	$('#floor_four').click(function(){
 		$('.floor_choice').removeClass('active');
 		$(this).addClass('active');
 		$('.floor_plan').slideUp();
 		$('#floor_plan_four').slideDown();
 	});
 	$('#floor_five').click(function(){
 		$('.floor_choice').removeClass('active');
 		$(this).addClass('active');
 		$('.floor_plan').slideUp();
 		$('#floor_plan_five').slideDown();
 	});
 	$('#floor_six').click(function(){
 		$('.floor_choice').removeClass('active');
 		$(this).addClass('active');
 		$('.floor_plan').slideUp();
 		$('#floor_plan_six').slideDown();
 	});

 	$('.floor_choice').mouseover(function(){
 		$(this).css('opacity', '1');
 	});

 	$('.floor_choice').mouseleave(function(){
 		$(this).css('opacity', '.3');
 	})



 	// Picture overlay
 	$('.overlay').mouseover(function(){
 		$(this).css('opacity', '1');
 	});

 	$('.overlay').mouseleave(function(){
 		$(this).css('opacity', '0');
 	});




// / initial timeout redirect homepage
        // var initial = null;

        // function invoke() {
        //     initial = window.setTimeout(
        //         function() {
        //             window.location.href = '/';
        //         }, 35000);
        // }

        // invoke();

        // $('body').on('click mousemove', function(){
        //     window.clearTimeout(initial);
        //     invoke();
        // })




});