<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>
</div> <!-- .body_content-->
    <footer>
      <nav>
            <?php
				wp_nav_menu( array(
					'menu_id'        => 'nav',
				) );
			?>
      </nav>

      <p>Copyright © 2017</p>
    </footer>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/main.js"></script>

<?php wp_footer(); ?>

</body>
</html>
