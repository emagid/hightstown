<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>

      


    <section class='hero rentals_hero'>
      <!-- <div class='slick_slide slide1'></div>
      <div class='slick_slide slide2'></div> -->
    </section>


    <h1>Floor Plans</h1>
    <div class="container">
      <div class='floor_plans'>
        <div id='floor_plan_one' class='floor_plan'>
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/floor1.png">
        </div>
        <div id='floor_plan_two' class='floor_plan'>
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/floor2.png">
        </div>
        <div id='floor_plan_three' class='floor_plan'>
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/floor3.png">
        </div>
        <div id='floor_plan_four' class='floor_plan'>
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/floor4.png">
        </div>
        <div id='floor_plan_five' class='floor_plan'>
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/floor5.png">
        </div>
        <div id='floor_plan_six' class='floor_plan'>
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/floor6.png">
        </div>
      </div>

      <div class='floor_choices'>
        <div id='floor_one' class='floor_choice'>
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/floor1.png">
        </div>
        <div id='floor_two' class='floor_choice'>
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/floor2.png">
        </div>
        <div id='floor_three' class='floor_choice'>
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/floor3.png">
        </div>
        <div id='floor_four' class='floor_choice'>
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/floor4.png">
        </div>
        <div id='floor_five' class='floor_choice'>
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/floor5.png">
        </div>
        <div id='floor_six' class='floor_choice'>
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/floor6.png">
        </div>
      </div>
      
    </div>

    
    <section class='contact_grey'>
      <p><strong>Share this page</strong></p>
      <a href="http://www.facebook.com/share.php?u=http://www.deerfieldwesterlea.com/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/fb.jpg"></a>
      <a href="http://plus.google.com/share?url=http://www.deerfieldwesterlea.com/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/g.jpg"></a>
      <a href="http://www.twitter.com/home?status=http://www.deerfieldwesterlea.com/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/twitter.jpg"></a>
      <a href="http://www.linkedin.com/shareArticle?mini=true&url=http://www.deerfieldwesterlea.com/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/in.jpg"></a>
    </section>

<?php
get_footer();
