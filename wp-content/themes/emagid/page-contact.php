<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>

    <section class='hero rentals_hero'>
      <!-- <div class='slick_slide slide1'></div>
      <div class='slick_slide slide2'></div> -->
    </section>


    <h1>Contact</h1>
    <div class='contact container'>
      <p>Are you interested in learning more about what we have to offer you? We are more than happy to provide additional information about our rentals. For any questions you have, contact us through the form below.
Please be sure to include your name, email, phone, and your questions. You will receive a confirmation after submitting the form, and we will respond as quickly as possible for your satisfaction.</p>
      <p>Are you interested in learning more about what we have to offer you? We are more than happy to provide additional information about our rentals. For any questions you have, contact us through the form below.
Please be sure to include your name, email, phone, and your questions. You will receive a confirmation after submitting the form, and we will respond as quickly as possible for your satisfaction.</p>

    <div class='contact_holder bordered'>
      <div class='contact_info'>
        <h3>Phone:</h3>
        <p>(844) 749-6850</p>
      </div>
      <div class='contact_info'>
        <h3>Hours of Operation:</h3>
        <p>Monday – Friday, 9:00 a.m. – 5:00 p.m.</p>
      </div>
      <div class='contact_info'>
        <h3>Email:</h3>
        <p>info@deerfieldwesterlea.com</p>
      </div>
    </div>
        
     <div class="contact_form">
    <p>We look forward to hearing from you. Contact us by phone, email, or the form below today!</p>
        

        <form>
            <?php echo do_shortcode('[contact-form-7 id="33" title="Contact Form"]'); ?>
        </form>
        </div>
    </div>

    
    <section class='contact_grey'>
      <p><strong>Share this page</strong></p>
      <a href="http://www.facebook.com/share.php?u=http://www.deerfieldwesterlea.com/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/fb.jpg"></a>
      <a href="http://plus.google.com/share?url=http://www.deerfieldwesterlea.com/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/g.jpg"></a>
      <a href="http://www.twitter.com/home?status=http://www.deerfieldwesterlea.com/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/twitter.jpg"></a>
      <a href="http://www.linkedin.com/shareArticle?mini=true&url=http://www.deerfieldwesterlea.com/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/in.jpg"></a>
    </section>


<?php
get_footer();
