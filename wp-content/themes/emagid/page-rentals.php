<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>

      

      <section class='hero rentals_hero'>
        <!-- <div class='slick_slide slide1'></div>
        <div class='slick_slide slide2'></div> -->
      </section>


      <h1>Affordable Apartment Rentals</h1>
      <div class="container">
      <section class='rentals'>
        <section class='left'>
          <p>Experience comfortable family living with Hightstown Development Associates. We offer affordable apartment rentals in the garden style community of Hightstown, New Jersey. Our complex was built in 1967 and renovated in 1998. The property itself consists of 25 two-story buildings set in a large, beautifully landscaped area. The property is located off South Main Street, which has historical character, with an active "Main Street" retail and entertainment area, beautiful homes and the famous Private Peddie School.</p>

          <h2>Location, Location, Location</h2>
          <ul>
            <li>Immediate access to New Jersey Tpke, via Exit 8, therefore within commuting distance of New York City, New Jersey and Philadelphia</li>
            <li>Close to the Princeton Junction Station, with direct trains to New York City and Philadelphia</li>
            <li>Convenient to the New Jersey Transit Train Station, at Exit 3B of I-195, with service to New York City and New Jersey</li>
          </ul>

          <h2>Apartment Details</h2>
          <ul>
            <li>145 One-Bedroom, One Bathroom Units – starting at $1,100.00</li>
            <li>54 Two-Bedroom, One Bathroom Units – starting at $1,300.00</li>
            <li>One Year Leases Available</li>
            <li>Security Deposit Is a Month & a Half of Rent</li>
            <li>Credit Check Required</li>
            <li>No Pets Allowed</li>
          </ul>

          <h2>Amenities</h2>
          <ul>
            <li>Main Floor & Upstairs Units Available</li>
            <li>On-Site Laundry Room Available</li>
            <li>Stove & Refrigerator Included</li>
            <li>On-Site Supervisor Available</li>
            <li>Heat, Hot Water and Gas Included with Rent</li>
            <li>On-Site Parking</li>
            <li>Hardwood Floors</li>
            <li>Bike Racks Available</li>
          </ul>

          

          <p class='contact_text'><a href="/contact">Contact us</a> at (844) 749-6850 to learn more about our affordable, one & two-bedroom apartments.</p>
        </section>

        <aside>
        <div class='home_photos'> 
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/rental.jpg">
        </div>
      </aside>
    </section>
        
    </div>

    
    <section class='contact_grey'>
      <p><strong>Share this page</strong></p>
      <a href="http://www.facebook.com/share.php?u=http://www.deerfieldwesterlea.com/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/fb.jpg"></a>
      <a href="http://plus.google.com/share?url=http://www.deerfieldwesterlea.com/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/g.jpg"></a>
      <a href="http://www.twitter.com/home?status=http://www.deerfieldwesterlea.com/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/twitter.jpg"></a>
      <a href="http://www.linkedin.com/shareArticle?mini=true&url=http://www.deerfieldwesterlea.com/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/in.jpg"></a>
    </section>

<?php
get_footer();
