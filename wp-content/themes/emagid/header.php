<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/hightstown.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/libs/slick/slick.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/libs/slick/slick.min.js"></script>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    
    <div class="sticky_header">
    
    
    <section class='header_top'>
      <p>Office: (609) 448-1933</p>
      <p>Toll Free: (844) 749-6850</p>
      <p>10 Westerlea Avenue Hightstown, NJ 08520</p>
    </section>

    <header>
      <a href="/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png"></a>
      <nav>
            <?php
				wp_nav_menu( array(
					'menu_id'        => 'header-nav',
				) );
			?>
      </nav>

      <div id="hamburger">
        <div class="bar1 hamburger_line"></div>
        <div class="bar2 hamburger_line"></div>
        <div class="bar3 hamburger_line"></div>
      </div>

      <div class='header_dropdown'>
            <?php
				wp_nav_menu( array(
					'menu_id'        => 'mobile-nav',
				) );
			?>
          <p class='address'>Office: (609) 448-1933</p>
          <p>Toll Free: (844) 749-6850</p>
          <p>10 Westerlea Avenue Hightstown, NJ 08520</p>
      </div>
    </header>
        
        </div>
    
    <div class="body_content">
    
    
