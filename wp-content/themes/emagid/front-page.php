<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>

 <section class='hero home_hero'>
      <!-- <div class='slick_slide slide1'></div>
      <div class='slick_slide slide2'></div> -->
    </section>

    <h1>Family-Friendly one & Two-Bedroom Apartments</h1>
    <div class='home-section container'>
      <section class='left'>
        <h2 class='no_top_margin'>Our Story</h2>
        <p>Welcome home! Hightstown Development Associates offers clean, comfortable one and two-bedroom apartments in the garden community of Hightstown, New Jersey. We are one of the largest and safest apartment complexes in town, offering a great, family-friendly environment located near many celebrated schools. Backed by more than 30 years of experience, our fully trained staff offers on-site management and maintenance services for your total peace of mind.</p>

        <section class='bordered'>
          <h3>Serving Hightstown, Princeton, East Windsor, Mercer County, & All of New Jersey</h3>
        </section>

        <p class='contact_text'><a href="/contact/">Contact us</a> at (844) 749-6850 to learn more about our affordable, one & two-bedroom apartments.</p>
      </section>

      <aside>
      <div class='home_photos'> 
        <a href="/rentals/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/rentals.png"></a>
        <a href="/pictures/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pictures.png"></a>
        <a><img src="<?php echo get_template_directory_uri(); ?>/assets/images/coupon.png"></a>
      </div>
    </aside>
      
    </div>

    
    <section class='contact_grey'>
      <p><strong>Share this page</strong></p>
      <a href="http://www.facebook.com/share.php?u=http://www.deerfieldwesterlea.com/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/fb.jpg"></a>
      <a href="http://plus.google.com/share?url=http://www.deerfieldwesterlea.com/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/g.jpg"></a>
      <a href="http://www.twitter.com/home?status=http://www.deerfieldwesterlea.com/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/twitter.jpg"></a>
      <a href="http://www.linkedin.com/shareArticle?mini=true&url=http://www.deerfieldwesterlea.com/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/in.jpg"></a>
    </section>

<?php
get_footer();
