<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>

    <section class='hero rentals_hero'>
      <!-- <div class='slick_slide slide1'></div>
      <div class='slick_slide slide2'></div> -->
    </section>


    <h1>Location</h1>
    <main class='location'>
      <h2 class='center'>Visit us in Hightstown, New Jersey, to take a tour of our one- or two-bedroom apartments.</h2>
      <p class='center'>10 Westerlea Avenue</p><p class='center'>Hightstown, NJ 08520</p>
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6168.0894691547455!2d-74.53534036325678!3d40.26370559137744!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c3df10ac4ad109%3A0xdba741e9fa1bb99c!2s10+Westerlea+Ave%2C+Hightstown%2C+NJ+08520!5e0!3m2!1sen!2sus!4v1511819503531" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
      <a href="https://www.google.com/maps/place/10+Westerlea+Ave,+Hightstown,+NJ+08520/@40.2637056,-74.5353404,16z/data=!4m5!3m4!1s0x89c3df10ac4ad109:0xdba741e9fa1bb99c!8m2!3d40.264676!4d-74.5290136"><button class='button'>DIRECTIONS</button></a>
    </main>

    
    <section class='contact_grey'>
      <p><strong>Share this page</strong></p>
      <a href="http://www.facebook.com/share.php?u=http://www.deerfieldwesterlea.com/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/fb.jpg"></a>
      <a href="http://plus.google.com/share?url=http://www.deerfieldwesterlea.com/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/g.jpg"></a>
      <a href="http://www.twitter.com/home?status=http://www.deerfieldwesterlea.com/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/twitter.jpg"></a>
      <a href="http://www.linkedin.com/shareArticle?mini=true&url=http://www.deerfieldwesterlea.com/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/in.jpg"></a>
    </section>

<?php
get_footer();
